<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'ParseController@index')->name('/');
Route::get('/search', 'ParseController@search')->name('search');

//ajax
Route::post('/start-parse', 'ParseController@startParse');
Route::post('/save-csv', 'ParseController@saveCSV');
Route::post('/save-db', 'ParseController@saveDB');
Route::post('/send-email', 'ParseController@sendMail');
Route::post('/search', 'ParseController@searchDate');
