<div class="list-group">

    <div class="list-group-item header-item active">
        <div class="number"><div class="item-text">#</div></div>
        <div class="date"><div class="item-text">date</div></div>
        <div class="time"><div class="item-text">time</div></div>
        <div class="title"><div class="item-text">title</div></div>
        <div class="description"><div class="item-text">description</div></div>
        <div class="image"><div class="item-text">image</div></div>
    </div>

    <div class="users-data">
        @php
        $i = 1;
        @endphp
        @foreach ($data as $article)
            <div class="list-group-item">
                <div class="number">{{$i}}</div>
                <div class="date">{{$article['parse_time']}}</div>
                <div class="time">{{$article['news_time']}}</div>
                <div class="title"><a href="{{$article['url']}}">{{$article['name']}}</a></div>
                <div class="description">{{$article['description']}}</div>
                <div class="image"><img src="{{$article['tags']}}" class="img-fluid"></div>
            </div>
            @php
                $i++;
            @endphp
        @endforeach
    </div>


</div>