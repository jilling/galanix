@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Search page</h1>

            <select class="form-control col-md-3" id="date">
                <option value="0">Select time</option>
                @foreach($data as $time=>$arr)
                    <option value="{{$time}}">{{$time}}</option>
                @endforeach
            </select>
            <div class="results"></div>
        </div>
    </div>
</div>
@endsection
