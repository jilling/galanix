@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Main page</h1>
            <input type="text" class="form-control col-md-2" id="quantity" name="quantity" placeholder="quantity for parse">

            <div class="results"></div>
            <div class="navigation">
                <button class="btn btn-primary" id="start">Start</button>
                <button class="btn btn-primary" id="clear">Clear</button>

                <button class="btn btn-primary" id="save-csv">Save in CSV</button>
                <a href="#" class="btn btn-primary" id="download-csv">Download CSV</a>
                <button class="btn btn-primary" id="save-db">Save in DB</button>

                <input type="email" class="form-control col-md-3" name="email" id="email" placeholder="Email">
                <button class="btn btn-primary" id="send-email">Send to email</button>
            </div>
        </div>
    </div>
</div>
@endsection
