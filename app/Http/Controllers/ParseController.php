<?php

namespace App\Http\Controllers;

use App\Parse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;
use PHPMailer\PHPMailer\PHPMailer;


class ParseController extends Controller
{
    public function index() {

        return view('index') ;

    }

    public function search() {

        $data = Parse::get(['parse_time'])->groupBy('parse_time');
        return view('search', ['data'=>$data]) ;

    }

    public function searchDate (Request $request) {

        if ($request->input('date') != 0) {
            $data = Parse::where('parse_time', '=', $request->input('date'))
                ->orderBy('parse_time', 'asc')
                ->get();
            return view('resultTable', ['data'=>$data]);
        }

    }

    public function startParse (Request $request) {

        $data = array();

        if ($request->input('quantity') == 0 || empty($request->input('quantity'))) {
            $quantity = null;
        } else {
            $quantity = $request->input('quantity');
        }

        $response = Curl::to('http://interfax.com.ua/')->get();
        $doc = \phpQuery::newDocument($response);
        $news = $doc->find('.articles-list .article')->slice( 0, $quantity );

        foreach ($news as $article) {
            $pq = pq($article);

            $response_detail = Curl::to('http://interfax.com.ua/'.$pq->find('.article-link')->attr('href'))->get();
            $doc_detail = \phpQuery::newDocument($response_detail);

            $data[] = array(
                'parse_time' => Carbon::now(),
                'news_time' => $pq->find('.article-time')->text(),
                'name' => $pq->find('.article-link')->text(),
                'url' => $pq->find('.article-link')->attr('href'),
                'description' => $doc_detail->find('.article-content p')->text(),
                'tags' => $doc_detail->find('.article-content-image')->attr('src')
            );
        }

        return view('resultTable', ["data"=>$data]);

    }

    public function saveCSV (Request $request) {

        $str = '';

        if ($request->input('quantity') == 0 || empty($request->input('quantity'))) {
            $quantity = null;
        } else {
            $quantity = $request->input('quantity');
        }

        $response = Curl::to('http://interfax.com.ua/')->get();
        $doc = \phpQuery::newDocument($response);
        $news = $doc->find('.articles-list .article')->slice( 0, $quantity );

        foreach ($news as $article) {
            $pq = pq($article);

            $response_detail = Curl::to('http://interfax.com.ua/'.$pq->find('.article-link')->attr('href'))->get();
            $doc_detail = \phpQuery::newDocument($response_detail);

            $str .= Carbon::now().';'.$pq->find('.article-time')->text().';'.$pq->find('.article-link')->text().';'.$doc_detail->find('.article-content p')->text().';'.$doc_detail->find('.article-content-image')->attr('src').';\r\n';

        }

        $fileName = time();
        $fp = fopen($fileName.'.csv', "w");
        fwrite($fp, $str);
        fclose($fp);
        chmod($fileName.'.csv', 0755);

        return $fileName.'.csv';

    }

    public function saveDB (Request $request) {
        $data = array();

        if ($request->input('quantity') == 0 || empty($request->input('quantity'))) {
            $quantity = null;
        } else {
            $quantity = $request->input('quantity');
        }

        $response = Curl::to('http://interfax.com.ua/')->get();
        $doc = \phpQuery::newDocument($response);
        $news = $doc->find('.articles-list .article')->slice( 0, $quantity );

        foreach ($news as $article) {

            $pq = pq($article);

            $response_detail = Curl::to('http://interfax.com.ua/'.$pq->find('.article-link')->attr('href'))->get();
            $doc_detail = \phpQuery::newDocument($response_detail);

            $data[] = array(
                "parse_time" => Carbon::now(),
                "news_time" => $pq->find('.article-time')->text(),
                "name" => $pq->find('.article-link')->text(),
                "description" => $doc_detail->find('.article-content p')->text(),
                "url" => 'http://interfax.com.ua/'.$pq->find('.article-link')->attr('href'),
                "tags" => $doc_detail->find('.article-content-image')->attr('src')
            );

        }

        Parse::insert($data);
    }

    public function sendMail (Request $request) {
        $mail = new PHPMailer();
        $mail->AddAddress($request->input('email'));
        $mail->SetFrom('test@test.com', 'Test');
        $mail->AddAttachment($request->input('file'));
        $mail->AddReplyTo("test@test.com","Test");
        $mail->MsgHTML("This is CSV file.");
        $mail->Subject = "Send CSV file";
        if(!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }
    }


}
