;
$( document ).ready(function() {
    $('#start').on('click', function (e) {
        e.preventDefault();
        $('.results').html('<img src="../images/loading.gif" class="img-fluid">');
        $.ajax({
            type: "POST",
            url: 'start-parse',
            data: { quantity: $('#quantity').val() },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( result ) {
                $('.results').html(result);
                $('#save-csv, #save-db').show();
            }
        });
    });

    $('#clear').on('click', function (e) {
        e.preventDefault();
        $('.results').html('');
    });

    $('#save-csv').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'save-csv',
            data: { quantity: $('#quantity').val() },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( result ) {
                $('#download-csv').attr('href', result);
                $('#save-csv').hide();
                $('#download-csv, #send-email, #email').show();
            }
        });
    });

    $('#save-db').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: 'save-db',
            data: { quantity: $('#quantity').val() },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function() {
                $('#save-db').hide();
            }
        });
    });

    $('#send-email').on('click', function (e) {
        e.preventDefault();
        $('#email').removeClass('error-input');

        if (isEmail($('#email').val())) {
            $.ajax({
                type: "POST",
                url: 'send-email',
                data: { email: $('#email').val(), file: $('#download-csv').attr('href') },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function( result ) {
                    $('#email, #send-email').hide();
                    console.log(result);
                }
            });
        } else {
            $('#email').addClass('error-input');
        }

    });

    $('#date').on('change', function (e) {
        e.preventDefault();
        $('.results').html('');

        $.ajax({
            type: "POST",
            url: 'search',
            data: { date: $(this).val() },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( result ) {
                $('.results').html(result);
            }
        });
    });


});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}